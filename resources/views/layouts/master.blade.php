<!DOCTYPE html>
<html>
    <head>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"/>
        <title>@yield('title')</title>
        <meta charset="UTF-8"/>
        <meta lang="es"/>        
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="icon" href="{{url('images/ic_launcher.png')}}">
        {!! MaterializeCSS::include_css() !!}
        @yield('css')
        <link href="{{url('css/layout.css')}}" rel="stylesheet" type="text/css"/>
    </head>        
    <body>
        <header>
            <div class="fixed-announcement grey darken-3">
                <div class="announcement">
                    <div class="container center">
                        <div class="center-align">
                            <span class="title-bar">@yield('text-center')</span>
                        </div>               
                    </div>
                </div>
                <nav class="top-nav orange">           
                    <div class="container">                    
                        <div class="nav-wrapper">
                            <a href="" class="button-collapse top-nav full hide-on-large-only" data-activates='menu'>
                                <i class="material-icons small">menu</i>
                            </a>
                            <div class="center-align">
                                <span class="subtitle">@yield('subtitle')</span>
                            </div>
                        </div>

                    </div>               
                </nav>
                <ul class="side-nav fixed light-green lighten-5" id="menu"> 
                    <li><div class="user-view">
                            <div class="background">
                                <img class="responsive-img" src="{{url('images/android.jpg')}}">
                            </div>                        
                            <span class="white-text name">APP STORE</span>
                            <span class="white-text email">SELECCIONA UNA OPCIÓN</span>
                        </div></li>
                    <li>
                        <a href="{{route('home')}}" class="waves-effect  waves-orange"><i class="material-icons small">home</i>
                            <span class="">INICIO<span>
                                    </a>
                                    </li>
                                    <li>
                                        <a href="{{route('users')}}"class="waves-effect  waves-orange"><i class="material-icons small">account_circle</i>
                                            <span class="">GESTIÓN DE USUARIOS<span> 
                                                    </a>
                                                    </li>
                                                    <li>
                                                        <a href="{{route('products')}}" class="waves-effect  waves-orange"><i class="material-icons small">local_grocery_store</i>
                                                            <span class="">GESTIÓN DE PRODUCTOS<span>
                                                                    </a>
                                                                    </li>                        
                                                                    </ul>            
                                                                    </header>
                                                                    <main class="container">   
                                                                        <div class="center-align content">@yield('content')</div>                                                        
                                                                    </main>
                                                                    <footer class="page-footer orange lighten-3">
                                                                        <div class="container">
                                                                            <div class="row">
                                                                                <div class="col l6 m6 s12">
                                                                                    <p class="center-align">APP STORE</p>
                                                                                </div>
                                                                                <div class="col l6 m6 s12">
                                                                                    <p class="center-align">Sitio Web para APP STORE</p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </footer>


                                                                    <script src="{{url('/js/jquery.min.js')}}" type="text/javascript"></script>
                                                                    {!! MaterializeCSS::include_js() !!}
                                                                    <script src="{{url('/js/menu.js')}}" type="text/javascript"></script>
                                                                    @yield('javascript')
                                                                    </body>
                                                                    </html>
                                                                                                                                       