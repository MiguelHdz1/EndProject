@extends('layouts.master')
@section('title', 'Productos')
@section('text-center', 'Productos')
@section('subtitle', 'Gestión de productos')
@section('content')
<div class="row">
    <div class="col s12 l8 m8 offset-l2 offset-m2">
        <table class="table-responsive striped">
            <thead>
            <th>Nombre</th>
            <th>Precio</th>
            <th><div class="center-align">Acciones</div></th>
            </thead>
            <tbody>
                @foreach($products as $product)
                <tr>
                    <td>{{$product->name}}</td>
                    <td>{{$product->price}}</td>
                    <td>
                        <div class="center-align">
                            <a class="waves-effect waves-light btn tooltipped light-blue darken-4"  data-position="top" data-delay="50" data-tooltip="Editar"><i class="material-icons">edit</i></a>
                            <a class="waves-effect waves-light btn tooltipped red darken-3"  data-position="top" data-delay="50" data-tooltip="Eliminar"><i class="material-icons">delete</i></a>
                        </div>                        
                    </td>
                </tr>
                @endforeach
            </tbody>    
            </tbody>
            <tfoot>
                <tr>        
                    <td colspan="3">                        
                        {{$products->render()}}                            
                    </td>        
                </tr>
            </tfoot>
        </table>
    </div>
    <div class="fixed-action-btn">
        <a class="btn-floating btn-large waves-effect waves-light blue">
            <i class="large material-icons">add</i>
        </a>    
    </div>
</div>
@stop