@extends('layouts.master')
@section('title', 'Usuarios')
@section('text-center', 'Usuarios')
@section('subtitle', 'Gestión de usuarios')
@section('content')
<div class="row">
    <div class="col s12 l8 m8 offset-l2 offset-m2">
        <table class="table-responsive striped">
            <thead>
            <th>Nombre</th>
            <th>Correo</th>
            <th><div class="center-align">Acciones</div></th>
            </thead>
            <tbody>
                @foreach($users as $user)
                <tr>
                    <td>{{$user->full_name}}</td>
                    <td>{{$user->email}}</td>
                    <td>
                        <div class="center-align">
                            <a href="{{route('users.edit',['id'=>$user->id])}}" class="waves-effect waves-light btn tooltipped light-blue darken-4"  data-position="top" data-delay="50" data-tooltip="Editar"><i class="material-icons">edit</i></a>
                            <a data-bind="{{$user->id}}" class="waves-effect waves-light btn tooltipped red darken-3 btn-delete"  data-position="top" data-delay="50" data-tooltip="Eliminar"><i class="material-icons">delete</i></a>
                        </div>                        
                    </td>
                </tr>
                @endforeach
            </tbody>    
            <tfoot>    
            </tfoot>
        </table>
    </div>
</div>
<div class="fixed-action-btn">
    <a href="{{route('users.create')}}" class="btn-floating btn-large waves-effect waves-light blue">
        <i class="large material-icons">add</i>
    </a>    
</div>
<form class="form_delete" method="POST" action="{{route('users.delete')}}">
    {{csrf_field()}}
    <input type="hidden" value="" name="id" id="id">
</form>
@stop