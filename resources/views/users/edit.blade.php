@extends('layouts.master')
@section('title', 'Gestión de usuarios')
@section('text-center', 'Gestión de usuarios')
@section('subtitle', 'Editar usuario')
@section('content')
<div class="row">
    <div class="col s12 l6 offset-l3 m8 offset-m2">
        <form method="POST" action="{{route('users.update')}}">
            {{csrf_field()}}
            <input type="hidden" value="{{$user->id}}" name="id" id="id">
            <div class="card">
                <div class="card-content">
                    <div class="card-title">Datos del Usuario</div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input value="{{$user->name}}" id="name" name="name" type="text">
                            <label for="name">Nombre</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s6">
                            <input value="{{$user->first_name}}" id="second_name" name="second_name" type="text">
                            <label for="second_name">Apellido paterno</label>
                        </div>
                        <div class="input-field col s6">
                            <input value="{{$user->last_name}}" id="last_name" name="last_name" type="text">
                            <label for="last_name">Apellido materno</label>
                        </div>
                    </div>                    
                    <div class="row">                    
                        <div class="col s8 offset-l2">                        
                            <div class="center-align">     
                                @php                                
                                switch($user->gender){
                                case 'M':
                                @endphp
                                <input checked="true" name="gender" value="M" type="radio" id="male" />
                                <label for="male">Masculino</label>
                                <input name="gender" value="F" type="radio" id="female" />
                                <label for="female">Femenino</label>
                                @php  
                                break;
                                case 'F':
                                @endphp
                                <input name="gender" value="M" type="radio" id="male" />
                                <label for="male">Masculino</label>
                                <input checked="true" name="gender" value="F" type="radio" id="female" />
                                <label for="female">Femenino</label>
                                @php 
                                break;
                                }
                                @endphp
                                

                                
                            </div>
                        </div>
                    </div>
                    <div class="right-align">
                        <a href="{{route('users')}}" class="waves-effect waves-light btn red darken-3">Cancelar</a>
                        <button class="waves-effect waves-light btn light-blue darken-4">Guardar</button>
                    </div>
                </div>
            </div>        
        </form>
    </div>        
</div>
@stop