@extends('layouts.master')
@section('title', 'Bienvenido')
@section('text-center', 'Bienvenido')
@section('subtitle', 'APP STORE')
@section('content')
<div class="slider">
    <ul class="slides">        
        <li>            
            <img src="{{url('images/woman.jpg')}}" class="responsive-img"> <!-- random image -->
            <div class="caption center-align">
                <h3>APP STORE</h3>
                <h5 class="light grey-text text-lighten-3">La aplicación de ventas con un diseño amigable para el usuario.</h5>            
            </div>
        </li>        
        <li>
            <img src="{{url('images/arm.jpg')}}" class="responsive-img"> <!-- random image -->
            <div class="caption right-align">
                <h3>UTILÍZALA DONDE QUIERAS.</h3>
                <h5 class="light grey-text text-lighten-3">Pruébala en tu Smartphone o Tablet</h5>
            </div>
        </li>
        <li>
            <img src="{{url('images/hipster.jpg')}}" class="responsive-img"> <!-- random image -->
            <div class="caption right-align">
                <h3>UNA APLICACIÓN QUE SABE LO QUE QUIERES</h3>
                <h5 class="light grey-text text-lighten-3">APP STORE es una aplicación inteligente.</h5>
            </div>
        </li>
        <li>
            <img src="{{url('images/money.jpg')}}" class="responsive-img"> <!-- random image -->
            <div class="caption left-align">
                <h3>DIFERENTES PRODUCTOS</h3>
                <h5 class="light grey-text text-lighten-3">Encuentra lo que buscas en diferentes categorías de productos.</h5>
            </div>
        </li>
    </ul>
</div>
@stop
@section('javascript')
<script src="{{url('/js/index.js')}}" type="text/javascript"></script>
@stop