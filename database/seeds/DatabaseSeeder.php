<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('users')->delete();
        DB::table('user_profiles')->delete();
        DB::table('categories')->delete();

        $user_profiles = [
            ['id' => 1, 'name' => 'Luis Miguel', 'first_name' => 'Ramos', 'last_name' => 'Hernandez', 'gender' => 'M'],
            ['id' => 2, 'name' => 'Rodrigo', 'first_name' => 'Garcia', 'last_name' => 'Ceballos', 'gender' => 'M'],
            ['id' => 3, 'name' => 'Alexis', 'first_name' => 'Neri', 'last_name' => 'Garcia', 'gender' => 'M'],
            ['id' => 4, 'name' => 'Jorge', 'first_name' => 'Negrete', 'last_name' => 'Paez', 'gender' => 'M'],
            ['id' => 5, 'name' => 'Marcos', 'first_name' => 'Tenorio', 'last_name' => 'Gonzalez', 'gender' => 'M']
        ];
        $users = [
            ['id' => 1, 'email' => 'luis@appdb.es', 'password' => bcrypt('123456789'), 'id_profile' => 1],
            ['id' => 2, 'email' => 'rodrigo@appdb.es', 'password' => bcrypt('123456789'), 'id_profile' => 2],
            ['id' => 3, 'email' => 'alexis@appdb.es', 'password' => bcrypt('123456789'), 'id_profile' => 3],
            ['id' => 4, 'email' => 'jorge@appdb.es', 'password' => bcrypt('123456789'), 'id_profile' => 4],
            ['id' => 5, 'email' => 'marcos@appdb.es', 'password' => bcrypt('123456789'), 'id_profile' => 5]
        ];

        $categories = [
            ['name' => 'Vehiculos, motos y otros'],
            ['name' => 'Alimentos y bebidas'],
            ['name' => 'Animales y mascotas'],
            ['name' => 'Arte'],
            ['name' => 'Bebés'],
            ['name' => 'Belleza y cuidado personal'],
            ['name' => 'Cámaras y accesorios'],
            ['name' => 'Celulares y telefonia'],
            ['name' => 'Coleccionables'],
            ['name' => 'Computación'],
            ['name' => 'Consolas y videojuegos'],
            ['name' => 'Deportes'],
            ['name' => 'Electrodomesticos'],
            ['name' => 'Electronica, Audio y video'],
            ['name' => 'Herramientas y Construcción'],
            ['name' => 'Hogar, muebles y jardín'],
            ['name' => 'Industrias y oficinas'],
            ['name' => 'Instrumentos musicales'],
            ['name' => 'Joyas y relojes'],
            ['name' => 'Juegos y juguetes'],
            ['name' => 'Libros, revistas y comics'],
            ['name' => 'Musica, Peliculas y series'],
            ['name' => 'Ropa, bolsas y calzado'],
            ['name' => 'Salud y equipamiento médico']
        ];
        DB::table('user_profiles')->insert($user_profiles);
        DB::table('users')->insert($users);
        DB::table('categories')->insert($categories);
    }

}
