<?php

use Illuminate\Http\Request;

Route::get('users', 'UsersController@index');
Route::post('users/save', 'UsersController@store');
Route::post('users/login', 'UsersController@logIn');
Route::get('users/get-interests/{id}', 'UsersController@InterestsUser');
Route::get('purchases/{id}', 'UsersController@GetPurchases');

Route::get('products/{id}', 'ProductsController@index');
Route::get('products/images/{url}', 'ProductsController@images');
Route::get('products/single-product/{id}', 'ProductsController@SingleProduct');
Route::get('products/product-searched/{id}/{id_product}', 'ProductsController@VerifyProduct');
Route::post('products/buy-product', 'ProductsController@buyElement');


Route::get('interest', 'InterestsController@index');
Route::get('interest/single-user/{id}/{id_product}', 'InterestsController@UserInterest');
Route::post('users/interests', 'InterestsController@store');
