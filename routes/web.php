<?php


Route::get('/', function () {
return view('index');})->name('home');

Route::get('web-users', 'WebUsersController@index')->name('users');
Route::get('web-users/create', 'WebUsersController@create')->name('users.create');
Route::post('web-users/store', 'WebUsersController@store')->name('users.store');
Route::post('web-users/delete', 'WebUsersController@delete')->name('users.delete');
Route::get('web-users/{id}/edit', 'WebUsersController@edit')->name('users.edit');
Route::post('web-users/update', 'WebUsersController@update')->name('users.update');

Route::get('web-products', 'WebProductsController@index')->name('products');

