<?php

namespace App\Http\Controllers;

use App\Entities\ProductsModel;
use App\Entities\VerifyProductModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;
use App\Entities\SalesModel;

class ProductsController extends Controller {

    public function index($id, Request $request) {
              $products = ProductsModel::select('products.id as id_product', 'products.img_url',
                'products.name', 'products.price', 'products.decription', 'categories.name as category',
                DB::raw('IFNULL(rating_count, 0) as rating_count'))->
                join('categories', 'products.id_categorie', '=', 'categories.id')
                ->leftJoin('rating_products', function($join) use ($id) {
            $join->on('rating_products.id_product', '=', 'products.id')
            ->where('rating_products.id_user', '=', $id)->where('products.status','!=','0');
        });
        $name = $request->input('name');
        if ($name != '') {
            $products->where('categories.name', 'like', '%' . $name . '%')->
                    orWhere('products.name', 'like', '%' . $name . '%');
        }
        return response()->json(["productos" => $products->orderBy('rating_count','desc')->orderBy('categories.name')->get()]);
    }
    
    public function Images($id) {
        $storagePath = storage_path('\\images\\' . $id);
        return Image::make($storagePath)->response();
    }

    public function SingleProduct($id) {
        $product = ProductsModel::select('products.id as id_product', 'products.img_url', 'products.name', 'products.price', 'products.decription', 'categories.name as category')->
                        join('categories', 'products.id_categorie', '=', 'categories.id')
                        ->where('products.id', '=', $id)->firstOrFail();
        return response()->json($product);
    }

    public function VerifyProduct($id, $id_product) {
        $state = false;
        $suggestion ='';
        $verify_product= VerifyProductModel::select('rating_count','id_product')
                ->where('id_user','=',$id)->where('id_product','=',$id_product);        
        if($verify_product->count()>0){
            $maxProduct = VerifyProductModel::select('rating_count','id_product')
                ->where('id_user','=',$id)->where('status','!=','0')->orderBy('rating_count','desc')->orderBy('updated_at','asc')->first();                       
            if(($verify_product->firstOrFail()->rating_count+1)>$maxProduct->rating_count){
                if($maxProduct->id_product!=$id_product){
                    $state= true;   
                    $category = ProductsModel::select('id_categorie')->where('id','=',$id_product)->first();
                    $suggestion = ProductsModel::select('name')->where('id_categorie','=',$category->id_categorie)
                            ->where('id','!=',$id_product)->where('status','!=','0')
                            ->orderByRaw('RAND()')->first();
                }                             
            }             
            $verify_product->update(['rating_count'=>$verify_product->firstOrFail()->rating_count+1]);                 
            return response()->json(["result" => $state,'suggestion'=>$suggestion]);
        }else{
            $new_rating = new VerifyProductModel();
            $new_rating->id_user = $id;
            $new_rating->id_product=$id_product;
            $new_rating->rating_count=1;
            $new_rating->save();
            return response()->json(["result" => $state,'suggestion'=>$suggestion]);
        }
    }   

    public function buyElement(Request $request) {
        $user = $request->input('id_user');
        $product = $request->input('id_product');
        
        $newProduct = new SalesModel();
        $newProduct->id_user=$user;
        $newProduct->id_product=$product;
        $newProduct->save();
        return response()->json(["result" => true]);
    }

}
