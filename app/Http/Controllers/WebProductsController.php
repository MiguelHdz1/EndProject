<?php

namespace App\Http\Controllers;

use App\Entities\ProductsModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class WebProductsController extends Controller {

    public function index(Request $request) {
        $products=ProductsModel::select('id','name','price')->where('status','!=','0')->paginate(5);
        return view('products/index')->with(['products'=>$products]);
    }

    public function create() {
        
    }

    public function store(Request $request) {
        
    }

}
