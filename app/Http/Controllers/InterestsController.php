<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Entities\CategoriesModel;
use App\Entities\UsersInterestsModel;

class InterestsController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $categories = CategoriesModel::select('id', 'name')->get();
        return response()->json(["categories" => $categories]);
    }
    
    public function UserInterest($id,$id_product){        
        $interest = UsersInterestsModel::select('id')
                ->where('id_user','=',$id)
                ->where('id_product','=',$id_product)->count();
        if($interest>0){
            return response()->json(["result" => true]);
        }else{
            return response()->json(["result" => false]);
        }
    }

    public function store(Request $request) {
        $interest = UsersInterestsModel::select('id')
                ->where('id_user','=',$request->id)
                ->where('id_product','=',$request->id_product);
        if($interest->count()>0){
            $interest->delete();
            return response()->json(["result" => false]);
        }else{
            $newInterest = new UsersInterestsModel();
            $newInterest->id_user=$request->id;
            $newInterest->id_product=$request->id_product;    
            $newInterest->save();
            return response()->json(["result" => true]);
        }
    }

}
