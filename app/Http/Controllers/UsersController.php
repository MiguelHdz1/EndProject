<?php

namespace App\Http\Controllers;

use App\Entities\LoginRecordsModel;
use App\Entities\UserProfilesModel;
use App\Entities\UsersModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Entities\UsersInterestsModel;
use App\Entities\SalesModel;

class UsersController extends Controller {

    public function index(Request $request) {
        $user = $request->input('id');
        $users = UsersModel::select('users.id', 'users.email', 'users.password',
                'user_profiles.name', 'user_profiles.first_name', 'user_profiles.last_name',
                'user_profiles.gender',\DB::raw('COUNT(sales.id_user) as sales_total')
                )->groupBy('sales.id_user')
                        ->leftJoin('user_profiles', 'users.id_profile', '=', 'user_profiles.id')
                        //->leftJoin('login_records', 'users.id', '=', 'login_records.id_user')
                        ->leftJoin('sales', 'users.id', '=', 'sales.id_user')
                        ->where('users.id', '=', $user);
        $session= LoginRecordsModel::select(\DB::raw('DATE_FORMAT(MAX(login_records.created_at),"%d-%m-%Y %H:%i") as last_session'))->where('id_user', '=', $user);
        


        return response()->json(["users" => $users->get(),"last_session"=>$session->get()]);
    }

    public function store(Request $request) {
        try {
            $user_profile = new UserProfilesModel();
            $user_profile->name = $request->input('name');
            $user_profile->first_name = $request->input('first_name');
            $user_profile->last_name = $request->input('last_name');
            $user_profile->gender = $request->input('gender');
            $user_profile->save();
            $user = new UsersModel();
            $user->email = $request->input('email');
            $user->password = $request->input('password');
            $user->id_profile = $user_profile->id;
            $user->save();
            return response()->json(['status' => true, "response" => 'Data saved'], 200);
        } catch (Exception $ex) {
            return response()->json("Coudn't save data" . $ex, 500);
        }
    }

    public function logIn(Request $request) {
        $userRequest = $request->input('email');
        $password = $request->input('password');
        $user = UsersModel::select('users.id', 'users.email', 'users.password', 'user_profiles.name', 'user_profiles.first_name', 'user_profiles.last_name', 'user_profiles.gender')
                ->leftJoin('user_profiles', 'users.id_profile', '=', 'user_profiles.id')->where('status','!=','0')
                ->where('users.email', '=', $userRequest);
        if ($user->count() > 0) {
            if (Hash::check($password,$user->firstOrFail()->password)) {
                $finalUser = $user;
                $logIn = new LoginRecordsModel();
                $logIn->id_user = $finalUser->firstOrFail()->id;
                $logIn->save();
                return response()->json(['status' => true,"id"=>$finalUser->firstOrFail()->id]);
            } else {
                return response()->json(['status' => false]);
            }
        } else {
            return response()->json(['status' => false]);
        }
    }
    
    public function InterestsUser($id){
        $interests = UsersInterestsModel::select('products.name','products.id','img_url')
                ->join('products','user_interests.id_product','=','products.id')
                ->where('id_user','=',$id)->orderBy('user_interests.created_at','desc')->get();
        return response()->json(['interest_user' => $interests]);
    }
    
    public function GetPurchases($id){
        $purchases = SalesModel::select('products.name','products.price','products.img_url',
                \DB::raw('DATE_FORMAT(sales.created_at,'
                . ' "%d-%m-%Y %H:%i") as date'))->where('sales.id_user','=',$id)
                ->leftJoin('products','products.id','=','sales.id_product')->orderBy('sales.created_at','desc')->get();
        return response()->json(['purchases' => $purchases]);
    }
}
