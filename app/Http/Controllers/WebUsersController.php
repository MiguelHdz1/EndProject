<?php

namespace App\Http\Controllers;

use App\Entities\UsersModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Entities\UserProfilesModel;

class WebUsersController extends Controller {

    public function index(Request $request) {
        $users = UsersModel::select(DB::raw('CONCAT(user_profiles.name," ",user_profiles.first_name) as full_name')
                ,'email','users.id')->where('users.status','!=','0')
                ->leftJoin('user_profiles','user_profiles.id','=','users.id_profile')->orderBy('users.created_at','desc')->get();        
        return view('users/index')->with(['users'=>$users]);
    }

    public function create() {
        $users = UsersModel::select(DB::raw('CONCAT(user_profiles.name," ",user_profiles.first_name) as full_name')
                ,'email','users.id')->where('users.status','!=','0')
                ->leftJoin('user_profiles','user_profiles.id','=','users.id_profile')->orderBy('users.created_at','desc')->get();        
        return view('users/create')->with(['users'=>$users]);
    }

    public function store(Request $request) {
        $profile = new UserProfilesModel();
        $profile->name=$request->name;
        $profile->first_name=$request->second_name;
        $profile->last_name=$request->last_name;
        $profile->gender=$request->gender;        
        $profile->save();        
        $user = new UsersModel();
        $user->email=$request->email;
        $user->password=  bcrypt($request->password);
        $user->id_profile= $profile->id;
        $user->save();
        return redirect('/web-users');
    }
    
    public function delete(Request $request){
        $user = UsersModel::findOrFail($request->id);
        $user->status='0'; 
        $user->save();
        return redirect('/web-users');
    }
    
    public function Edit($id){
        $user= UserProfilesModel::select('id','user_profiles.name','user_profiles.first_name','user_profiles.last_name',
                'user_profiles.gender')->findOrFail($id);
                return view('users/edit')->with(['user'=>$user]);
    }
    
    public function Update(Request $request){
        $profile = UserProfilesModel::findOrFail($request->id);
        $profile->name=$request->name;
        $profile->first_name=$request->second_name;
        $profile->last_name=$request->last_name;
        $profile->gender=$request->gender;        
        $profile->save();  
        return redirect('/web-users');
    }
}
