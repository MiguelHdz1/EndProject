<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class LoginRecordsModel extends Model
{
    protected $table='login_records';
    protected $fillable=['id_user'];
}
