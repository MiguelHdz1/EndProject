<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class CategoriesModel extends Model
{
    protected $table='categories';
    protected $fillable=['name'];
}
