<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class VerifyProductModel extends Model
{
    protected $table='rating_products';
    protected $fillable=['id_user','id_product','rating_count'];
}
