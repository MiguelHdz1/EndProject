<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class UserProfilesModel extends Model
{
    protected $table='user_profiles';
    protected $fillable=['name','first_name','last_name','gender'];
}
