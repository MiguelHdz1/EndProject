<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class UsersModel extends Model
{
    protected $table='users';
    protected $fillable=['email','password','id_profile'];
}
