<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class UsersInterestsModel extends Model
{
    protected $table='user_interests';
    protected $fillable=['id_user','id_categorie'];
}
