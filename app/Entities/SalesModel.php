<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class SalesModel extends Model
{
    protected $table='sales';
    protected $fillable=['id_user','id_product'];
}
