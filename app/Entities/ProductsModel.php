<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class ProductsModel extends Model
{
    protected $table='products';
    protected $fillable=['name','price','decription','id_categorie'];
}
